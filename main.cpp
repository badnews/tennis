//
//  main.cpp
//  tennis
//
//  Created by Rav Chandra on 21/01/13.
//  Copyright (c) 2013 Rav Chandra. All rights reserved.
//


#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <cassert>
#include <cstring>

using namespace std;


class Player
{
private:
    string name;
public:
    Player(string n): name(n) { }
    string getName() { return name; }
};


typedef vector<Player*> PlayerList;

class Match
{
protected:
    PlayerList players;
    int server_idx;
    Player* getServer() { return players[server_idx]; }
    Player* getOtherPlayer() { return players[(server_idx+1)&1]; }
    void switchServer() { server_idx = (server_idx+1)&1; }
public:
    Match(Player *p1, Player *p2) {
        players.push_back(p1);
        players.push_back(p2);
        server_idx = 0;
    }
    virtual Player* updateRound(Player *winner) = 0;
    virtual string getScore() = 0;
    virtual ~Match() {};
};


class DummyMatch : public Match
{
public:
    ~DummyMatch() { }
    DummyMatch(Player *p1, Player *p2): Match(p1, p2) { }
    string getScore() { return string("nil"); }
    Player* updateRound(Player *winner) {
        return NULL;
    }
};


typedef map<Player*, int> PointsTable;

class TennisMatch : public Match
{
private:
    int curr_point;
    int curr_game;
    int curr_set;
    PointsTable points_score;
    PointsTable game_scores[5];
    PointsTable set_score;
    
    void initPointsTable(PointsTable *pt) {
        PlayerList::const_iterator pitr;
        for (pitr = players.begin(); pitr != players.end(); pitr++)
            (*pt)[*pitr] = 0;
    }
    void initPoint() {
        curr_point = 0;
        initPointsTable(&points_score);
    }
    void initSet() {
        curr_game = 0;
        initPointsTable(&game_scores[curr_set]);
        initPoint();
    }
public:
    ~TennisMatch() { }
    TennisMatch(Player *p1, Player *p2): Match(p1, p2), curr_point(0), curr_game(0), curr_set(0) {
        initSet();
    }
    
    Player* updateRound(Player *winner) {
        // update scoring
        points_score[winner]++;
        
        int winner_score = points_score[winner];
        int other_score = curr_point - winner_score;
        
        if (winner_score < 4) {
            // still in current point
            curr_point++;
        } else if (winner_score - other_score >= 2) {
            // end of point
            initPoint();
            game_scores[curr_set][winner]++;
            curr_game++;
            
            int winner_game_score = game_scores[curr_set][winner];
            int other_game_score = curr_game - winner_game_score;
            
            if (winner_game_score < 6) {
                // still in current game
            } else if (winner_game_score - other_game_score >= 2) {
                // end of game
                set_score[winner]++;
                
                int winner_set_score = set_score[winner];
                int other_set_score = curr_set - winner_set_score;
                
                if (winner_set_score < 3) {
                    // next set
                    curr_set++;
                    initSet();
                } else if (winner_set_score - other_set_score >= 2) {
                    // end of match
                    return NULL;
                }
            }
            
            //cout << "Next game, winner: " << winner->getName()  << endl;
            
            // switch service at end of each point
            switchServer();
        }
        return getServer();
    }
    
    string getScore() {
        stringstream ss;
        Player *server = getServer();
        Player *other = getOtherPlayer();
        
        for (int i = 0; i <= curr_set; i++) {
            PointsTable *pt = &game_scores[i];
            int server_score = (*pt)[server];
            int other_score = (*pt)[other];
            ss << server_score << "-" << other_score;
            ss << " ";
        }
        ss << " point: " << points_score[server] << "-" << points_score[other];
        return ss.str();
    }
};


Match *createMatch(bool tennis, Player *p1, Player *p2)
{
    if (tennis) return new TennisMatch(p1, p2);
    else return new DummyMatch(p1, p2);
}

void deleteMatch(Match *m)
{
    delete m;
}

// using main function to simulate unit tests
int main(int argc, const char * argv[])
{
    Match* m;
    bool tennis = true;
    if (argc > 1) {
        // if any argument let's assume we are testing with DummyMatch
        tennis = false;
    }
    
    {
        cout << endl << "test case #1: Jane always wins!" << endl;
        
        Player *p1 = new Player("Bob");
        Player *p2 = new Player("Jane");
        
        m = createMatch(tennis, p1, p2);
        
        Player *nextServer;
        int i;
        for (i=1; i < 100; i++) {
            nextServer = m->updateRound(p2);
            string serverName = (nextServer == NULL) ? "end" : nextServer->getName();
            cout << "(" << i << "/" << serverName << ") score " << m->getScore() << endl;
            if (nextServer == NULL) break;
        }
        // should be 4*6*3 points played
        assert(i == 4*6*3);
        
        deleteMatch(m);
    }
    
    {
        cout << endl << "test case #2: alternate winners! -- no ending to match." << endl;
        
        Player *p1 = new Player("Bob");
        Player *p2 = new Player("Jane");
        m = createMatch(tennis, p1, p2);
        
        char winner_idx = 0;
        Player *nextServer;
        Player *winningPlayer;
        int i;
        for (i=1; i < 200; i++) {
            winningPlayer = (winner_idx++ & 1) ? p2 : p1;
            nextServer = m->updateRound(winningPlayer);
            string serverName = (nextServer == NULL) ? "end" : nextServer->getName();
            cout << "(" << i << "/" << winningPlayer->getName() << ") score " <<
            m->getScore() << " next service: " << serverName << endl;
            if (nextServer == NULL) break;
        }
        // NULL indicates game was completed
        assert(nextServer != NULL);
        // TODO: check score and ensure still on first game of first set
        deleteMatch(m);
    }
    
    {
        cout << endl << "test case #3: alternate winners after every 20 points" << endl;
        
        Player *p1 = new Player("Bob");
        Player *p2 = new Player("Jane");
        m = createMatch(tennis, p1, p2);
        
        char winner_idx = 0;
        Player *nextServer;
        Player *winningPlayer;
        int i;
        for (i=1; i < 300; i++) {
            if (i % 20 == 0) winner_idx++;
            winningPlayer = (winner_idx & 1) ? p2 : p1;
            nextServer = m->updateRound(winningPlayer);
            string serverName = (nextServer == NULL) ? "end" : nextServer->getName();
            cout << "(" << i << "/" << winningPlayer->getName() << ") score " <<
            m->getScore() << " next service: " << serverName << endl;
            if (nextServer == NULL) break;
        }
        // NULL indicates game was completed
        assert(nextServer == NULL);
        // Bob should have won!
        assert(winningPlayer == p1);
        deleteMatch(m);
    }
    
    {
        cout << endl << "test case #4: alterante on 6 points" << endl;
        
        Player *p1 = new Player("Bob");
        Player *p2 = new Player("Jane");
        m = createMatch(tennis, p1, p2);
        
        char winner_idx = 0;
        Player *nextServer;
        Player *winningPlayer;
        int i;
        for (i=1; i < 100; i++) {
            if (i % 6 == 0) winner_idx++;
            winningPlayer = (winner_idx & 1) ? p2 : p1;
            nextServer = m->updateRound(winningPlayer);
            string serverName = (nextServer == NULL) ? "end" : nextServer->getName();
            cout << "(" << i << "/" << winningPlayer->getName() << ") score " <<
            m->getScore() << " next service: " << serverName << endl;
            if (nextServer == NULL) break;
        }
        // NULL indicates game was completed
        assert(nextServer != NULL);
        string score = m->getScore();
        string expected = "8-9";
        assert(score.substr(0, expected.size()) == expected);
        deleteMatch(m);        
    }
    
    return 0;
}
